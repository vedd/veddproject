import pyrealsense2
import numpy as np
import cv2
from ultralytics import YOLO
import os

PIXEL_LENGTH = 640
PIXEL_WIDTH = 480
FRAMERATE = 30

def main():
	# file locations
	os.chdir("../")
	root_directory = os.getcwd()
	weights_location = root_directory + '/Transfer to ORIN/train4/weights/best.pt'

	context = pyrealsense2.context()
	devices = context.query_devices()
	# refresh devices
	for device in devices:
		device.hardware_reset()

	# create an object that handles all realsense devices
	pipeline = pyrealsense2.pipeline()
	config = pyrealsense2.config()

	# enable all connected cameras
	for device in devices:
		serial_number = device.get_info(pyrealsense2.camera_info.serial_number)
		print(f"enabling camera with serial number: {serial_number}")
		config.enable_device(serial_number)
		# configure the stream characteristics to start with the following settings
		config.enable_stream(pyrealsense2.stream.color, PIXEL_LENGTH, PIXEL_WIDTH, pyrealsense2.format.bgr8, FRAMERATE)
		config.enable_stream(pyrealsense2.stream.depth, PIXEL_LENGTH, PIXEL_WIDTH, pyrealsense2.format.z16, FRAMERATE)
	# format is in charge of how binary data is encoded

	pipeline.start(config)

	# aligining depth stream to rgb stream
	color_alignment = pyrealsense2.stream.color
	align = pyrealsense2.align(color_alignment)

	# yolo weights
	model = YOLO(os.path.join(root_directory,weights_location))

	# creating infinite loop for streaming
	while True:
		# wait for frames to become avalailable
		frame = pipeline.wait_for_frames()

		#aligned_frames = align.process(frame)
		color_frame = frame.get_color_frame()
		if color_frame:
			# convert images to np arrays
			# convert data to a numpy array
			#depth_frame = frame.get_depth_frame()
			#depth_image = np.asanyarray(depth_frame.get_data())
			color_image = np.asanyarray(color_frame.get_data())
			# display distance as a color inensity
			#depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha = 0.08), cv2.COLORMAP_JET)
			results = model(color_image, stream=True)

			boxes = None
			for result in results:
				boxes = result.boxes

				for box in boxes:
					box_copy = box.xyxy[0].to('cpu').detach().numpy().copy() # fetched vbox coordinates in top, left, bottom, right format
					confidence = box.cls

					top_left_coordinates = (int(box_copy[0]), int(box_copy[1]))
					bottom_right_coordinates = (int(box_copy[2]), int(box_copy[3]))
					color_red = (0,0,255)
					font_face = cv2.FONT_HERSHEY_SIMPLEX
					line_type = cv2.LINE_4

					cv2.rectangle(color_image, top_left_coordinates, bottom_right_coordinates, color_red, thickness=2, lineType=line_type)
					cv2.putText(color_image, text=model.names[int(confidence)], org=top_left_coordinates, fontFace=font_face, fontScale=0.7, color=color_red, thickness=2, lineType=line_type)
				
			annotated_frame = results[0].plot()
			cv2.imshow("color_image", annotated_frame)
			#cv2.imshow("depth_image", depth_colormap)

			if cv2.waitKey(1) == ord('q'):
				break

	pipeline.stop()

if __name__ == '__main__':
	main()